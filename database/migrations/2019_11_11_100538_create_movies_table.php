<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->longText('overview')->nullable();
            $table->string('vote_average')->nullable();
            $table->string('release_date')->nullable();
            $table->json('genre_ids')->nullable();
            $table->string('original_title')->nullable();
            $table->string('original_language')->nullable();
            $table->text('backdrop_path')->nullable();
            $table->boolean('adult')->nullable();
            $table->text('poster_path')->nullable();
            $table->boolean('video')->nullable();
            $table->integer('vote_count')->nullable();
            $table->integer('popularity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
