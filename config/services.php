<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 11/8/19
 * Time: 8:15 PM
 */
return [
    'tmdb' => [
        'secret' => env('TMDB_KEY'),
    ],
];
