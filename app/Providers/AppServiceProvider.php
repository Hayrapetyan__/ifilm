<?php

namespace App\Providers;

use App\Contracts\GenerInterface;
use App\Contracts\MovieInterface;
use App\Repositories\GenerRepository;
use App\Repositories\MovieRepositories;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GenerInterface::class,GenerRepository::class);
        $this->app->bind(MovieInterface::class,MovieRepositories::class);
    }
}
