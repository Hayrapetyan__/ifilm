<?php

namespace App\Console\Commands;


use App\Movies;
use Carbon\Carbon;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class MoviesUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'movies:update-movies';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get all movies';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('movies')->truncate();

        $client = new Client(['base_uri' => 'https://api.themoviedb.org/3/movie/popular']);
        $data = [];
        for($j = 1; $j <= 500; $j += 20) {
            for($i = $j; $i <= $j + 21; $i++) {
                if($i <= 500) {
                    $response = $client->request('GET', '?api_key='.config('services.tmdb.secret')."&language=ru-RU&page=$i");
                    $response = json_decode($response->getBody()->getContents(), true);
                    foreach ($response["results"] as $key => $qb) {
                        $data[] = [
                            'id' => isset($response["results"][$key]["id"]) ? $response["results"][$key]["id"] : null,
                            'title' => isset($response["results"][$key]["title"]) ? $response["results"][$key]["title"] : null,
                            'overview' => isset($response["results"][$key]["overview"]) ? $response["results"][$key]["overview"] : null,
                            'vote_average' => isset($response["results"][$key]["vote_average"]) ? $response["results"][$key]["vote_average"] : null,
                            'release_date' => isset($response["results"][$key]["release_date"]) ? $response["results"][$key]["release_date"] : null,
                            'genre_ids' => isset($response["results"][$key]["genre_ids"]) ? json_encode($response["results"][$key]["genre_ids"]) : null,
                            'original_title' => isset($response["results"][$key]["original_title"]) ? $response["results"][$key]["original_title"] : null,
                            'original_language' => isset($response["results"][$key]["original_language"]) ? $response["results"][$key]["original_language"] : null,
                            'backdrop_path' => isset($response["results"][$key]["backdrop_path"]) ? $response["results"][$key]["backdrop_path"] : null,
                            'adult' => isset($response["results"][$key]["adult"]) ? $response["results"][$key]["adult"] : null,
                            'poster_path' => isset($response["results"][$key]["poster_path"]) ? $response["results"][$key]["poster_path"] : null,
                            'video' => isset($response["results"][$key]["video"]) ? $response["results"][$key]["video"] : null,
                            'vote_count' => isset($response["results"][$key]["vote_count"]) ? $response["results"][$key]["vote_count"] : null,
                            'popularity' => isset($response["results"][$key]["popularity"]) ? $response["results"][$key]["popularity"] : null,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                    }
                }

            }
        }

        $data = collect($data)->unique('id');
        $data = $data->toArray();
        foreach (array_chunk($data,500) as $t) {
            Movies::insert($t);
        }

    }
}
