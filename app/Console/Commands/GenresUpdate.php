<?php
namespace App\Console\Commands;
use App\Genres;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class GenresUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'genres:update-genres';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get from api all news genres for films';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client(['base_uri' => 'https://api.themoviedb.org/3/genre/movie/']);
        $response = $client->request('GET', 'list?api_key='.config('services.tmdb.secret').'&language=ru-RU');
        $response = json_decode($response->getBody()->getContents(), true);
        $genreDataRu = [];
        $existentGenres = Genres::get();

        foreach ($response['genres'] as $key => $genre) {
            if ($existentGenres->isEmpty() || $existentGenres[$key]->genre_id !== $genre['id']) {
                $genreDataRu[] = [
                    'lang' => 'ru',
                    'genre_id' => $genre['id'],
                    'name' => $genre['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
        }
        Genres::insert($genreDataRu);
    }
}
