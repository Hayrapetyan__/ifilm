<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genres extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'genres';

    /**
     * @var array
     */
    protected $fillable = [
        'lang',
        'genre_id',
        'name'
    ];
}
