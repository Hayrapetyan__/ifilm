<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'movies';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'overview',
        'vote_average',
        'release_date',
        'genre_ids',
        'original_title',
        'original_language',
        'backdrop_path',
        'adult',
        'poster_path',
        'vote_count',
        'popularity',
        'video',
    ];
}
