<?php

namespace App\Repositories;


use App\Contracts\MovieInterface;
use App\Movies;
use Carbon\Carbon;
use GuzzleHttp\Client;

class MovieRepositories implements MovieInterface
{

    /**
     * @var Movies
     */
    private $model;

    public function __construct(Movies $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getPopular()
    {
        $startDate = Carbon::now()->subMonth(12);
        $endDate = Carbon::now()->subMonth(2);

        return $this->model->where('vote_average', '>=', 6)->where('release_date', '>=', $startDate)
            ->where('release_date', '<=', $endDate)->where('original_language', '=', 'en')
            ->where('popularity', '>=', 20)->orderBy('popularity', 'DESC')->select('id', 'title', 'poster_path')->get();
    }

    /**
     * @return mixed
     */
    public function getRecommendations()
    {
        $endDate = Carbon::now()->subMonth(6);

        return $this->model->where('popularity', '>=', 10)
            ->where('vote_average', '>=', '8')
            ->where('original_language', '!=', 'ja')
            ->where('original_language', '!=', 'ko')
            ->where('original_language', '!=', 'zh')
            ->where('release_date', '<=', $endDate)->inRandomOrder()->limit(4)->get();
    }

    /**
     * @return mixed|void
     */
    public function getPaginate($keyword, $genreId, $year)
    {
        if ($keyword == 'newest_movies') {
            $startDate = Carbon::now()->subMonth(6);

            return $this->model->where('vote_average', '>=', 6)->where('release_date', '>=', $startDate)
                ->orderBy('popularity', 'DESC')->paginate(12);
        } else {
            return $this->model->where('title', 'LIKE', '%' . $keyword . "%")
                ->where('genre_ids', 'LIKE', '%' . $genreId . "%")
                ->where('release_date', 'LIKE', '%' . $year . "%")
                ->orderBy('popularity', 'DESC')->paginate(12);
        }
    }

    /**
     * @return mixed
     */
    public function getTrailer()
    {
        $client = new Client(['base_uri' => 'https://api.themoviedb.org/3/movie/']);
        $response = $client->request('GET', 'popular?api_key='.config('services.tmdb.secret')."&language=ru-RU&page=1");
        $response = json_decode($response->getBody()->getContents(), true);
        $id = $response['results'][0]['id'];
        $videoResponse = $client->request('GET', "$id/videos?api_key=".config('services.tmdb.secret')."&language=ru-RU");
        $videoResponse = json_decode($videoResponse->getBody()->getContents(), true);
        $backfropResponse = $client->request('GET', "$id/images?api_key=".config('services.tmdb.secret')."&language=en-EN&include_image_language=null");
        $backfropResponse = json_decode($backfropResponse->getBody()->getContents(), true);
        $data = [
            $backfropResponse["backdrops"][0]["file_path"],
            $videoResponse["results"][0]["key"]
        ];
        return $data;
    }

    /**
     * @return mixed
     */
    public function getNowPlaying()
    {
        $startDate = Carbon::now()->subMonth(2)->subDay(12);
        $endDate = Carbon::now()->addDays(5);

        return $this->model->where('release_date', '>=', $startDate)
            ->where('release_date', '<=', $endDate)
            ->orderBy('popularity', 'DESC')->limit(4)->get();
    }

    /**
     * @return mixed
     */
    public function getMovie($id)
    {
        return $this->model->where('id', '=', $id)->get();
    }
}
