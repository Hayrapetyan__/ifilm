<?php

namespace App\Repositories;


use App\Contracts\GenerInterface;
use App\Genres;

class GenerRepository implements GenerInterface
{
    /**
     * @var Genres
     */
    private $model;

    public function __construct(Genres $model)
    {

        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->get();
    }
}
