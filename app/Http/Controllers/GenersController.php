<?php

namespace App\Http\Controllers;


use App\Contracts\GenerInterface;

class GenersController extends Controller
{
    /**
     * @var GenerInterface
     */
    private $generRepo;

    public function __construct(GenerInterface $generRepo)
    {
        $this->generRepo = $generRepo;
    }

    public function getAll()
    {
        $data = $this->generRepo->getAll();
        return response()->json(['data' => $data]);
    }
}
