<?php

namespace App\Http\Controllers;


use App\Contracts\MovieInterface;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    /**
     * @var MovieInterface
     */
    private $movieRepo;

    public function __construct(MovieInterface $movieRepo)
    {
        $this->movieRepo = $movieRepo;
    }

    /**
     * @return mixed
     */
    public function getPopular()
    {
        $data = $this->movieRepo->getPopular();
        return response()->json(['data' => $data]);
    }

    /**
     *
     */
    public function getRecommendation()
    {
        $data = $this->movieRepo->getRecommendations();
        return response()->json(['data' => $data]);
    }

    /**
     *
     */
    public function getPaginate(Request $request)
    {
        $keyword = $request->keyword;
        $genreId = $request->genres;
        $year = $request->year;
        $data = $this->movieRepo->getPaginate($keyword, $genreId, $year);
        return response()->json(['data' => $data]);
    }

    /**
     *
     */
    public function getLastPopularTrailer()
    {
        $data = $this->movieRepo->getTrailer();
        return response()->json(['data' => $data]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNowPlaying()
    {
        $data = $this->movieRepo->getNowPlaying();
        return response()->json(['data' => $data]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMovie(Request $request)
    {
        $data = $this->movieRepo->getMovie($request["id"]);
        return response()->json(['data' => $data->first()]);
    }
}
