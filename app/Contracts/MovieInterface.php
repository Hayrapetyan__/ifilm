<?php

namespace App\Contracts;


interface MovieInterface
{
    /**
     * @return mixed
     */
    public function getPopular();

    /**
     * @return mixed
     */
    public  function getRecommendations();

    /**
     * @return mixed
     */
    public function getPaginate($keyword, $genreId, $year);

    /**
     * @return mixed
     */
    public function getTrailer();

    /**
     * @return mixed
     */
    public function getNowPlaying();

    /**
     * @return mixed
     */
    public function getMovie($id);
}
