export default $axios => resource =>({
  index(id) {
    return $axios.$get(`/${resource}?id=${id}`)
  },
  paginatiMovies(params) {
    return $axios.$get((`/${resource}?page=${params.page}`), {params})
  }
})
