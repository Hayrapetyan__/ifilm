import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/images/404_poster.jpg',
  loading: '/images/poster_loader.gif',
  attempt: 1
})
