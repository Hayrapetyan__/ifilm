import createRepository from '../api/Repository'

export default (ctx, inject) => {
  const repository = createRepository(ctx.$axios)

  const repositories = {
    genres: repository('genres'),
    popular: repository('popular'),
    popularTrailer: repository('popular-trailer'),
    recommendation: repository('recommendation'),
    pagination: repository('paginate'),
    nowPlaying: repository('now-playing'),
    movie: repository('movie'),
  }

  inject('repo', repositories)
}
