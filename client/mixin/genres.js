import { mapGetters } from 'vuex'

export const genresParse = {
  methods: {
    genresShow(arr) {
      let ids = JSON.parse(arr)
      let items = []
      ids.forEach((id) => {
        let item = this.genres.find(element => element.genre_id == id)
        items.push(item)
      })
      return items
    },
  },
  computed: {
    ...mapGetters({
      genres: 'genres/get',
    }),
  },
  created() {
    this.$store.dispatch('genres/getAllGenres')
  }
}
