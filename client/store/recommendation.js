export const state = () => ({
  list: []
})

export const mutations = {
  updateList (state, item) {
    state.list = item
  }
}

export const actions = {
  async getRecommendation ({ commit }) {
    let _this = this
    const response = await _this.$repo.recommendation.index();
    commit('updateList', response.data)
  }
}

export const getters = {
  get (state) {
    return state.list
  }
}
