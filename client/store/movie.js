export const state = () => ({
  paginationMovies: [],
  movie: {}
})

export const mutations = {
  updateList (state, item) {
    state.paginationMovies = item
  },
  updateMovieItem (state, item) {
    state.movie = item
  }
}

export const actions = {
  async getMoviesPagination ({ commit }, params) {
    let _this = this
    const response = await _this.$repo.pagination.paginatiMovies(params);
    commit('updateList', response.data)
  },
  async getMovie ({ commit }, id) {
    let _this = this
    const response = await _this.$repo.movie.index(id);
    commit('updateMovieItem', response.data)
  }
}

export const getters = {
  get (state) {
    return state.paginationMovies
  },
  getMovie (state) {
    return state.movie
  }
}
