export const state = () => ({
  list: []
})

export const mutations = {
  updateList (state, title) {
    state.list = title
  }
}

export const actions = {
  async getAllGenres ({ commit }) {
      let _this = this
      const response = await _this.$repo.genres.index();
      commit('updateList', response.data)
  }
}

export const getters = {
  get (state) {
    return state.list
  }
}
