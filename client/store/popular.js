export const state = () => ({
  list: [],
  details: {
    trailerKey: '',
    backdropKey: ''
  },
})

export const mutations = {
  updateList (state, item) {
    state.list = item
  },
  updateTrailer (state, item) {
    state.details.trailerKey = item[1]
    state.details.backdropKey = item[0]
  }
}

export const actions = {
  async getPopular ({ commit }) {
    let _this = this
    const response = await _this.$repo.popular.index();
    commit('updateList', response.data)
  },
  async getTrailer ({ commit }) {
    let _this = this
    const response = await _this.$repo.popularTrailer.index();
    commit('updateTrailer', response.data)
  }
}

export const getters = {
  get (state) {
    return state.list
  },
  getTrailer (state) {
    return state.details
  }
}
