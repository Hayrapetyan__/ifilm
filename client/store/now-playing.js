export const state = () => ({
  list: []
})

export const mutations = {
  updateList (state, item) {
    state.list = item
  }
}

export const actions = {
  async getNowPlaying ({ commit }) {
    let _this = this
    const response = await _this.$repo.nowPlaying.index();
    commit('updateList', response.data)
  }
}

export const getters = {
  get (state) {
    return state.list
  }
}
