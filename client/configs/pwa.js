export default {
  manifest: {
    name: 'iFilm',
    short_name: 'iFilm',
    display: 'standalone',
    background_color: '#00C58E',
    theme_color: '#00C58E',
    description: 'Find artists for your events.'
  },
  workbox: {
    runtimeCaching: [
      { urlPattern: 'https://fonts.googleapis.com/.*' },
      { urlPattern: 'https://fonts.gstatic.com/.*' }
    ]
  }
}
