<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => '/api'], function () use ($router) {
    $router->get('/genres', 'GenersController@getAll');
    $router->get('/popular', 'MoviesController@getPopular');
    $router->get('/recommendation', 'MoviesController@getRecommendation');
    $router->get('/paginate', 'MoviesController@getPaginate');
    $router->get('/popular-trailer', 'MoviesController@getLastPopularTrailer');
    $router->get('/now-playing', 'MoviesController@getNowPlaying');
    $router->get('/movie', 'MoviesController@getMovie');
});
